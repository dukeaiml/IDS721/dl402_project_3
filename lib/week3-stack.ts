import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';
// Use AWS CDK to create an S3 bucket with versioning and encryption enabled.
import { Bucket, BucketEncryption }
    from 'aws-cdk-lib/aws-s3';

export class Week3Stack extends cdk.Stack {
    constructor(scope: Construct, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        new Bucket(this, 'XXXXXXXXXXXXX', {
            versioned: true,
            encryption: BucketEncryption.KMS_MANAGED,
        });
    }
}
