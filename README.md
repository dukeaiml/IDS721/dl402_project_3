# IDS 721 Mini Project 3
This project is to create an S3 Bucket using CDK with AWS CodeWhisperer. 

## Packages and Dependencies
To install CDK library:
`npm install aws-cdk-lib`

To install TypeScript:
`npm install -g typescript`

Also, to configure the IAM user identities so that it have the required policies, Add AmazonS3FullAccess, AmazonEC2ContainerRegistryFullAccess, IAMFullAccess, CloudFormation, and Systems Manager. 


## Build and deploy the project:
- Run `cdk init app --language typescript` to create a blank CDK project.
- Update the code in `lib/week3-stack.ts` file using AWS CodeWhisperer. 
- Run `npm run build` to build the project.
- Run `cdk bootstrap aws://<AWS account ID>/<Region>` to bootstrap the project to AWS.
- Run `cdk deploy` to deploy the CloudFormation template.
After these steps, the project is setup and the S3 bucket should be in your AWS console.

## AWS CodeWhisperer
- AWS CodeWhisperer is a ML tool that can generate codes for you automatically. You can simply type a comment describing the things you want to build and it can help finish the code.
- In this project, I used the prompt `// Use AWS CDK to create an S3 bucket with versioning and encryption enabled.` to make a S3 bucket that supports versioning and encryption. 
- The following is the screenshot of using CodeWhisperer
![Alt text](/screenshots/CodeWhisperer.png)

## Screenshots of S3 Bucket
- Screenshot of S3 bucket is created and has a versioning:
![Alt text](/screenshots/BucketVersion.png)
- Screenshot of CloudFormation is setup:
![Alt text](/screenshots/CloudFormation.png)